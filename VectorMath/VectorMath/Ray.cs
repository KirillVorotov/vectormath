﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VectorMath
{
    [Serializable]
    public struct Ray
    {
        private Vector3f origin;
        private Vector3f direction;

        public Ray(Vector3f origin, Vector3f direction)
        {
            this.origin = origin;
            this.direction = direction;
        }

        public Vector3f Origin
        {
            get
            {
                return origin;
            }
            set
            {
                origin = value;
            }
        }

        public Vector3f Direction
        {
            get
            {
                return direction;
            }
            set
            {
                direction = value;
            }
        }
    }
}
