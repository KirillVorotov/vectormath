﻿using System;

namespace VectorMath
{
    [Serializable]
    public struct Bezier2f : IEquatable<Bezier2f>
    {
        private Vector3f P0;
        private Vector3f P1;
        private Vector3f P2;

        public Bezier2f(Vector3f Point0, Vector3f Point1, Vector3f Point2)
        {
            P0 = Point0;
            P1 = Point1;
            P2 = Point2;
        }

        public Vector3f Point0
        {
            get { return P0; }
            set
            {
                P0 = value;
            }
        }

        public Vector3f Point1
        {
            get { return P1; }
            set
            {
                P1 = value;
            }
        }

        public Vector3f Point2
        {
            get { return P2; }
            set
            {
                P2 = value;
            }
        }

        public Vector3f this[int index]
        {
            get
            {
                if (index == 0) return P0;
                else if (index == 1) return P1;
                else if (index == 2) return P2;
                throw new IndexOutOfRangeException("You tried to access this Bezier curve at index: " + index);
            }
            set
            {
                if (index == 0) P0 = value;
                else if (index == 1) P1 = value;
                else if (index == 2) P2 = value;
                throw new IndexOutOfRangeException("You tried to access this Bezier curve at index: " + index);
            }
        }

        public static Vector3f Point(Bezier2f curve, float t)
        {
            if (t < 0f)
                t = 0f;
            if (t > 1f)
                t = 1f;
            return (1f - t) * (1f - t) * curve.P0 + 2f * t * (1f - t) * curve.P1 + t * t * curve.P2;
        }

        public static Vector3f FirstDerivative(Bezier2f curve, float t)
        {
            if (t < 0f)
                t = 0f;
            if (t > 1f)
                t = 1f;
            return (2f * t - 2f) * curve.P0 + (2f - 4f * t) * curve.P1 + 2f * t * curve.P2;
        }

        public static Bezier2f[] SplitAt(Bezier2f curve, float t)
        {
            if (t < 0f)
                t = 0f;
            if (t > 1f)
                t = 1f;

            var p01 = curve.P0 * (1f - t) + curve.P1 * t;
            var p12 = curve.P1 * (1f - t) + curve.P2 * t;

            var pMiddle = curve.Point(t);

            return new[]
            {
                new Bezier2f(curve.P0, p01, pMiddle),
                new Bezier2f(pMiddle, p12, curve.P2)
            };
        }

        public static float Length(Bezier2f curve)
        {
            var A0 = curve.P1 - curve.P0;
            var A1 = curve.P0 - 2f * curve.P1 + curve.P2;

            if (A1 != Vector3f.Zero)
            {
                var c = 4f * Vector3f.Dot(A1, A1);
                var b = 8f * Vector3f.Dot(A0, A1);
                var a = 4f * Vector3f.Dot(A0, A0);
                var q = 4f * a * c - b * b;
                var twoCpB = 2f * c + b;
                var sumCBA = c + b + a;
                var mult0 = 0.25f / c;
                var mult1 = q / (8f * Math.Pow(c, 1.5f));
                var result = mult0 * (twoCpB * Math.Sqrt(sumCBA) - b * Math.Sqrt(a)) + mult1 * (Math.Log(2 * Math.Sqrt(c * sumCBA) + twoCpB) - Math.Log(2 * Math.Sqrt(c * a) + b));
                return (float)result;
            }
            else
            {
                return 2f * A0.Length;
            }
        }

        public static float InterpolatedLength(Bezier2f curve, float interpolationPrecision = 0.05f)
        {
            if (interpolationPrecision < 0.0001f)
            {
                interpolationPrecision = 0.0001f;
            }
            var dt = interpolationPrecision / (curve.P2 - curve.P0).Length;
            var l = 0f;
            for (float t = dt; t < 1f; t += dt)
            {
                l += (curve.Point(t - dt) - curve.Point(t)).Length;
            }
            return l;
        }

        public Vector3f Point(float t)
        {
            return Point(this, t);
        }

        public Vector3f FirstDerivative(float t)
        {
            return FirstDerivative(this, t);
        }

        public Bezier2f[] SplitAt(float t)
        {
            return SplitAt(this, t);
        }

        public float Length()
        {
            return Length(this);
        }

        public float InterpolatedLength(float interpolationPrecision = 0.05f)
        {
            return InterpolatedLength(this, interpolationPrecision);
        }

        public override string ToString()
        {
            return String.Format("{0}{3} {1}{3} {2}", P0.ToString(), P1.ToString(), P2.ToString(), System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this.P0.GetHashCode();
                hashCode = (hashCode * 397) ^ this.P1.GetHashCode();
                hashCode = (hashCode * 397) ^ this.P2.GetHashCode();
                return hashCode;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Bezier2f))
                return false;

            return this.Equals((Bezier2f)obj);
        }

        public bool Equals(Bezier2f other)
        {
            return
                P0 == other.P0 &&
                P1 == other.P1 &&
                P2 == other.P2;
        }

        #region Operators

        /// <summary>
        /// Translates an instance in a given direction.
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="vector"></param>
        public static Bezier2f operator +(Bezier2f curve, Vector3f vector)
        {
            curve.P0 += vector;
            curve.P1 += vector;
            curve.P2 += vector;
            return curve;
        }

        /// <summary>
        /// Translates an instance in a given direction.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="curve"></param>
        public static Bezier2f operator +(Vector3f vector, Bezier2f curve)
        {
            curve.P0 += vector;
            curve.P1 += vector;
            curve.P2 += vector;
            return curve;
        }

        /// <summary>
        /// Translates an instance in a direction opposite to the given.
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="vector"></param>
        public static Bezier2f operator -(Bezier2f curve, Vector3f vector)
        {
            curve.P0 -= vector;
            curve.P1 -= vector;
            curve.P2 -= vector;
            return curve;
        }

        /// <summary>
        /// Translates an instance in a direction opposite to the given.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="curve"></param>
        public static Bezier2f operator -(Vector3f vector, Bezier2f curve)
        {
            curve.P0 -= vector;
            curve.P1 -= vector;
            curve.P2 -= vector;
            return curve;
        }

        /// <summary>
        /// Multiplies an instance by a scalar.
        /// </summary>
        /// <param name="curve">The instance.</param>
        /// <param name="scale">The scalar.</param>
        /// <returns>The result of the calculation.</returns>
        public static Bezier2f operator *(Bezier2f curve, float scale)
        {
            curve.P0 *= scale;
            curve.P1 *= scale;
            curve.P2 *= scale;
            return curve;
        }

        /// <summary>
        /// Multiplies an instance by a scalar.
        /// </summary>
        /// <param name="scale">The scalar.</param>
        /// /// <param name="curve">The instance.</param>
        /// <returns>The result of the calculation.</returns>
        public static Bezier2f operator *(float scale, Bezier2f curve)
        {
            curve.P0 *= scale;
            curve.P1 *= scale;
            curve.P2 *= scale;
            return curve;
        }

        /// <summary>
        /// Divides an instance by a scalar.
        /// </summary>
        /// <param name="curve">The instance.</param>
        /// <param name="scale">The scalar.</param>
        /// <returns>The result of the calculation.</returns>
        public static Bezier2f operator /(Bezier2f curve, float scale)
        {
            float mult = 1.0f / scale;
            curve.P0 *= mult;
            curve.P1 *= mult;
            curve.P2 *= mult;
            return curve;
        }

        /// <summary>
        /// Compares two instances for equality.
        /// </summary>
        /// <param name="left">The first instance.</param>
        /// <param name="right">The second instance.</param>
        /// <returns>True, if left equals right; false otherwise.</returns>
        public static bool operator ==(Bezier2f left, Bezier2f right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Compares two instances for inequality.
        /// </summary>
        /// <param name="left">The first instance.</param>
        /// <param name="right">The second instance.</param>
        /// <returns>True, if left does not equa lright; false otherwise.</returns>
        public static bool operator !=(Bezier2f left, Bezier2f right)
        {
            return !left.Equals(right);
        }

        #endregion
    }
}
