﻿using System;

namespace VectorMath
{
    [Serializable]
    public struct Bezier3f : IEquatable<Bezier3f>
    {
        private Vector3f P0;
        private Vector3f P1;
        private Vector3f P2;
        private Vector3f P3;

        private Vector3f A;
        private Vector3f B;
        private Vector3f C;

        public Bezier3f(Vector3f point0, Vector3f point1, Vector3f point2, Vector3f point3)
        {
            P0 = point0;
            P1 = point1;
            P2 = point2;
            P3 = point3;

            A = 3f * (P0 - P1);
            B = 3f * (P0 - 2f * P1 + P2);
            C = P0 - 3f * P1 + 3f * P2 - P3;
        }

        public Vector3f Point0
        {
            get { return P0; }
            set
            {
                P0 = value;
                CalculateHelperVectors();
            }
        }

        public Vector3f Point1
        {
            get { return P1; }
            set
            {
                P1 = value;
                CalculateHelperVectors();
            }
        }

        public Vector3f Point2
        {
            get { return P2; }
            set
            {
                P2 = value;
                CalculateHelperVectors();
            }
        }

        public Vector3f Point3
        {
            get { return P3; }
            set
            {
                P3 = value;
                CalculateHelperVectors();
            }
        }

        public Vector3f this[int index]
        {
            get
            {
                if (index == 0) return P0;
                else if (index == 1) return P1;
                else if (index == 2) return P2;
                else if (index == 3) return P3;
                throw new IndexOutOfRangeException("You tried to access this Bezier curve at index: " + index);
            }
            set
            {
                if (index == 0)
                {
                    P0 = value;
                    CalculateHelperVectors();
                    return;
                }
                else if (index == 1)
                {
                    P1 = value;
                    CalculateHelperVectors();
                    return;
                }
                else if (index == 2)
                {
                    P2 = value;
                    CalculateHelperVectors();
                    return;
                }
                else if (index == 3)
                {
                    P3 = value;
                    CalculateHelperVectors();
                    return;
                }
                throw new IndexOutOfRangeException("You tried to access this Bezier curve at index: " + index);
            }
        }

        public static Vector3f Point(Bezier3f curve, float t)
        {
            if (t < 0f)
                t = 0f;
            if (t > 1f)
                t = 1f;
            //return (1f - t) * (1f - t) * (1f - t) * curve.P0 + 3f * t * (1f - t) * (1f - t) * curve.P1 + 3f * t * t * (1f - t) * curve.P2 + t * t * t * curve.P3;
            return curve.P0 - curve.A * t + curve.B * t * t - curve.C * t * t * t;
        }

        public static Vector3f FirstDerivative(Bezier3f curve, float t)
        {
            if (t < 0f)
                t = 0f;
            if (t > 1f)
                t = 1f;
            return -3f * curve.P0 * (1f - t) * (1f - t) + curve.P1 * (3f * (1f - t) * (1f - t) - 6f * (1f - t) * t) + curve.P2 * (6f * (1f - t) * t - 3f * t * t) + 3f * curve.P3 * t * t;
        }

        public static Bezier3f[] SplitAt(Bezier3f curve, float t)
        {
            if (t < 0f)
                t = 0f;
            if (t > 1f)
                t = 1f;

            var p01 = curve.P0 * (1f - t) + curve.P1 * t;
            var p12 = curve.P1 * (1f - t) + curve.P2 * t;
            var p23 = curve.P2 * (1f - t) + curve.P3 * t;

            var p012 = p01 * (1f - t) + p12 * t;
            var p123 = p12 * (1f - t) + p12 * t;

            var pMiddle = curve.Point(t);

            return new[]
            {
                new Bezier3f(curve.P0, p01, p012, pMiddle), new Bezier3f(pMiddle, p123, p23, curve.P3)
            };
        }

        public static float Length(Bezier3f curve)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the full length of the given curve.
        /// </summary>
        /// <param name="curve">Given Bezier3f curve.</param>
        /// <param name="interpolationPrecision">Distance between sampled points.</param>
        /// <returns>Returns the full length of the given curve.</returns>
        public static float InterpolatedLength(Bezier3f curve, float interpolationPrecision = 0.05f)
        {
            if (interpolationPrecision < 0.0001f)
            {
                interpolationPrecision = 0.0001f;
            }
            var dt = interpolationPrecision / (curve.P3 - curve.P0).Length;
            var l = 0f;
            for (float t = dt; t < 1; t += dt)
            {
                l += (curve.Point(t - dt) - curve.Point(t)).Length;
            }
            return l;
        }

        public static float ClosestPoint(Bezier3f curve, Vector3f point)
        {
            throw new NotImplementedException();
        }

        public Vector3f Point(float t)
        {
            return Point(this, t);
        }

        public Vector3f FirstDerivative(float t)
        {
            return FirstDerivative(this, t);
        }

        public Bezier3f[] SplitAt(float t)
        {
            return SplitAt(this, t);
        }

        public float Length()
        {
            return Length(this);
        }

        /// <summary>
        /// Returns the full length of the given curve.
        /// </summary>
        /// <param name="interpolationPrecision">Distance between sampled points.</param>
        /// <returns>Returns the full length of the given curve.</returns>
        public float InterpolatedLength(float interpolationPrecision = 0.05f)
        {
            return InterpolatedLength(this, interpolationPrecision);
        }

        public float ClosestPoint(Vector3f point)
        {
            return ClosestPoint(this, point);
        }

        public override string ToString()
        {
            return String.Format("{0}{4} {1}{4} {2}{4} {3}", P0.ToString(), P1.ToString(), P2.ToString(), P3.ToString(), System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this.P0.GetHashCode();
                hashCode = (hashCode * 397) ^ this.P1.GetHashCode();
                hashCode = (hashCode * 397) ^ this.P2.GetHashCode();
                hashCode = (hashCode * 397) ^ this.P3.GetHashCode();
                return hashCode;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Bezier3f))
                return false;

            return this.Equals((Bezier3f)obj);
        }

        public bool Equals(Bezier3f other)
        {
            return
                P0 == other.P0 &&
                P1 == other.P1 &&
                P2 == other.P2 &&
                P3 == other.P3;
        }

        private void CalculateHelperVectors()
        {
            A = 3f * (P0 - P1);
            B = 3f * (P0 - 2f * P1 + P2);
            C = P0 - 3f * P1 + 3f * P2 - P3;
        }

        #region Operators

        /// <summary>
        /// Translates an instance in a given direction.
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="vector"></param>
        public static Bezier3f operator +(Bezier3f curve, Vector3f vector)
        {
            curve.P0 += vector;
            curve.P1 += vector;
            curve.P2 += vector;
            curve.P3 += vector;
            curve.CalculateHelperVectors();
            return curve;
        }

        /// <summary>
        /// Translates an instance in a given direction.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="curve"></param>
        public static Bezier3f operator +(Vector3f vector, Bezier3f curve)
        {
            curve.P0 += vector;
            curve.P1 += vector;
            curve.P2 += vector;
            curve.P3 += vector;
            curve.CalculateHelperVectors();
            return curve;
        }

        /// <summary>
        /// Translates an instance in a direction opposite to the given.
        /// </summary>
        /// <param name="curve"></param>
        /// <param name="vector"></param>
        public static Bezier3f operator -(Bezier3f curve, Vector3f vector)
        {
            curve.P0 -= vector;
            curve.P1 -= vector;
            curve.P2 -= vector;
            curve.P3 -= vector;
            curve.CalculateHelperVectors();
            return curve;
        }

        /// <summary>
        /// Translates an instance in a direction opposite to the given.
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="curve"></param>
        public static Bezier3f operator -(Vector3f vector, Bezier3f curve)
        {
            curve.P0 -= vector;
            curve.P1 -= vector;
            curve.P2 -= vector;
            curve.P3 -= vector;
            curve.CalculateHelperVectors();
            return curve;
        }

        /// <summary>
        /// Multiplies an instance by a scalar.
        /// </summary>
        /// <param name="curve">The instance.</param>
        /// <param name="scale">The scalar.</param>
        /// <returns>The result of the calculation.</returns>
        public static Bezier3f operator *(Bezier3f curve, float scale)
        {
            curve.P0 *= scale;
            curve.P1 *= scale;
            curve.P2 *= scale;
            curve.P3 *= scale;
            curve.CalculateHelperVectors();
            return curve;
        }

        /// <summary>
        /// Multiplies an instance by a scalar.
        /// </summary>
        /// <param name="scale">The scalar.</param>
        /// /// <param name="curve">The instance.</param>
        /// <returns>The result of the calculation.</returns>
        public static Bezier3f operator *(float scale, Bezier3f curve)
        {
            curve.P0 *= scale;
            curve.P1 *= scale;
            curve.P2 *= scale;
            curve.P3 *= scale;
            curve.CalculateHelperVectors();
            return curve;
        }

        /// <summary>
        /// Divides an instance by a scalar.
        /// </summary>
        /// <param name="curve">The instance.</param>
        /// <param name="scale">The scalar.</param>
        /// <returns>The result of the calculation.</returns>
        public static Bezier3f operator /(Bezier3f curve, float scale)
        {
            float mult = 1.0f / scale;
            curve.P0 *= mult;
            curve.P1 *= mult;
            curve.P2 *= mult;
            curve.P3 *= mult;
            curve.CalculateHelperVectors();
            return curve;
        }

        /// <summary>
        /// Compares two instances for equality.
        /// </summary>
        /// <param name="left">The first instance.</param>
        /// <param name="right">The second instance.</param>
        /// <returns>True, if left equals right; false otherwise.</returns>
        public static bool operator ==(Bezier3f left, Bezier3f right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Compares two instances for inequality.
        /// </summary>
        /// <param name="left">The first instance.</param>
        /// <param name="right">The second instance.</param>
        /// <returns>True, if left does not equa lright; false otherwise.</returns>
        public static bool operator !=(Bezier3f left, Bezier3f right)
        {
            return !left.Equals(right);
        }

        public static explicit operator Bezier3f(Line3f line)
        {
            return new Bezier3f(line.StartPoint, line.Point(1f / 3f), line.Point(2f / 3f), line.EndPoint);
        }

        public static explicit operator Bezier3f(Bezier2f curve)
        {
            return new Bezier3f(curve.Point0, curve.Point0 + 2f / 3f * (curve.Point1 - curve.Point0), curve.Point2 + 2f / 3f * (curve.Point1 - curve.Point2), curve.Point2);
        }

        #endregion
    }
}
