﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VectorMath
{
    [Serializable]
    public struct AABB : IEquatable<AABB>
    {
        private static readonly string listSeparator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;

        private Vector3f center;
        private Vector3f halfwidth;

        public AABB(Vector3f center, Vector3f halfwidth)
        {
            this.center = center;
            this.halfwidth = halfwidth;
        }

        public Vector3f Center
        {
            get
            {
                return center;
            }
            set
            {
                center = value;
            }
        }

        public Vector3f Halfwidth
        {
            get
            {
                return halfwidth;
            }
            set
            {
                halfwidth = new Vector3f(Math.Abs(value.X), Math.Abs(value.Y), Math.Abs(value.Z));
            }
        }

        public Vector3f MinPoint
        {
            get
            {
                return center - halfwidth;
            }
            set
            {
                var maxPoint = center + halfwidth;
                center = 0.5f * (value + maxPoint);
                halfwidth = (maxPoint - value) * 0.5f;
            }
        }

        public Vector3f MaxPoint
        {
            get
            {
                return center + halfwidth;
            }
            set
            {
                var minPoint = center - halfwidth;
                center = 0.5f * (minPoint + value);
                halfwidth = (value - minPoint) * 0.5f;
            }
        }

        public static AABB CreateFromMinMaxPoints(Vector3f minPoint, Vector3f maxPoint)
        {
            return new AABB()
            {
                center = 0.5f * (minPoint + maxPoint),
                halfwidth = (maxPoint - minPoint) * 0.5f
            };
        }

        public static bool Intersects(AABB first, AABB second)
        {
            if (Math.Abs(first.center.X - second.center.X) > (Math.Abs(first.halfwidth.X) + Math.Abs(second.halfwidth.X)))
                return false;
            if (Math.Abs(first.center.Y - second.center.Y) > (Math.Abs(first.halfwidth.Y) + Math.Abs(second.halfwidth.Y)))
                return false;
            if (Math.Abs(first.center.Z - second.center.Z) > (Math.Abs(first.halfwidth.Z) + Math.Abs(second.halfwidth.Z)))
                return false;
            return true;
        }

        public static bool Contains(AABB aabb, Vector3f point)
        {
            if (point.X > Math.Max(aabb.MinPoint.X, aabb.MaxPoint.X))
                return false;
            if (point.X < Math.Min(aabb.MinPoint.X, aabb.MaxPoint.X))
                return false;

            if (point.Y > Math.Max(aabb.MinPoint.Y, aabb.MaxPoint.Y))
                return false;
            if (point.Y < Math.Min(aabb.MinPoint.Y, aabb.MaxPoint.Y))
                return false;

            if (point.Z > Math.Max(aabb.MinPoint.Z, aabb.MaxPoint.Z))
                return false;
            if (point.Z < Math.Min(aabb.MinPoint.Z, aabb.MaxPoint.Z))
                return false;

            return true;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is AABB))
                return false;

            return this.Equals((AABB)obj);
        }

        public bool Equals(AABB other)
        {
            return
                center == other.center &&
                halfwidth == other.halfwidth;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("Center: {0}{2} Halfwidth: {1}", center, halfwidth, listSeparator);
        }
    }
}
