﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VectorMath
{
    [Serializable]
    public class Polygon2f
    {
        private List<Vector2f> points;
        private Vector2f min;
        private Vector2f max;
        
        public Vector2f Min
        {
            get
            {
                return min;
            }
        }

        public Vector2f Max
        {
            get
            {
                return max;
            }
        }

        public Polygon2f()
        {
            this.points = new List<Vector2f>();
            min = new Vector2f(float.MinValue, float.MinValue);
            max = new Vector2f(float.MaxValue, float.MaxValue);
        }

        public Polygon2f(List<Vector2f> points)
        {
            this.points = points;
            min = FindMin();
            max = FindMax();
        }

        public Polygon2f(Vector2f[] points)
        {
            this.points = new List<Vector2f>();
            this.points.AddRange(points);
            min = FindMin();
            max = FindMax();
        }

        public void AddPointLast(Vector2f point)
        {
            points.Add(point);
            if (point.X < min.X)
                min.X = point.X;
            if (point.X > max.X)
                max.X = point.X;
            if (point.Y < min.Y)
                min.Y = point.Y;
            if (point.Y > max.Y)
                max.Y = point.Y;
        }

        public void AddPointFirst(Vector2f point)
        {
            points.Insert(0, point);
            if (point.X < min.X)
                min.X = point.X;
            if (point.X > max.X)
                max.X = point.X;
            if (point.Y < min.Y)
                min.Y = point.Y;
            if (point.Y > max.Y)
                max.Y = point.Y;
        }

        public void InsertPointAt(int index, Vector2f point)
        {
            points.Insert(index, point);
            if (point.X < min.X)
                min.X = point.X;
            if (point.X > max.X)
                max.X = point.X;
            if (point.Y < min.Y)
                min.Y = point.Y;
            if (point.Y > max.Y)
                max.Y = point.Y;
        }

        public void RemovePointAt(int index)
        {
            points.RemoveAt(index);
            min = FindMin();
            max = FindMax();
        }

        public void Clear()
        {
            points.Clear();
            min = new Vector2f(float.MinValue, float.MinValue);
            max = new Vector2f(float.MaxValue, float.MaxValue);
        }

        public bool Contains(Vector2f point)
        {
            return Contains(this, point);
        }

        public static bool Contains(Polygon2f polygon, Vector2f point)
        {
            bool inside = false;
            for (int i = 0, j = polygon.points.Count - 1; i < polygon.points.Count; j = i++)
            {
                if ((polygon.points[i].Y > point.Y) != (polygon.points[j].Y > point.Y) &&
                     point.X < (polygon.points[j].X - polygon.points[i].X) * (point.Y - polygon.points[i].Y) / (polygon.points[j].Y - polygon.points[i].Y) + polygon.points[i].X)
                {
                    inside = !inside;
                }
            }
            return inside;
        }

        private Vector2f FindMin()
        {
            float minX = float.MaxValue, minY = float.MaxValue;
            for (int i = 0; i < points.Count; i++)
            {
                if (points[i].X < minX)
                    minX = points[i].X;
                if (points[i].Y < minY)
                    minY = points[i].Y;
            }
            return new Vector2f(minX, minY);
        }

        private Vector2f FindMax()
        {
            float maxX = float.MinValue, maxY = float.MinValue;
            for (int i = 0; i < points.Count; i++)
            {
                if (points[i].X > maxX)
                    maxX = points[i].X;
                if (points[i].Y > maxY)
                    maxY = points[i].Y;
            }
            return new Vector2f(maxX, maxY);
        }
    }
}
