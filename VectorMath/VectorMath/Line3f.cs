﻿using System;

namespace VectorMath
{
    [Serializable]
    public struct Line3f : IEquatable<Line3f>
    {
        private static readonly string listSeparator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;

        private Vector3f P0;
        private Vector3f P1;

        public Line3f(Vector3f StartPoint, Vector3f EndPoint)
        {
            P0 = StartPoint;
            P1 = EndPoint;
        }

        public Vector3f StartPoint
        {
            get { return P0; }
            set { P0 = value; }
        }

        public Vector3f EndPoint
        {
            get { return P1; }
            set { P1 = value; }
        }

        public static Vector3f Point(Line3f line, float t)
        {
            if (t < 0f)
                t = 0f;
            if (t > 1f)
                t = 1f;
            return line.P0 * (1f - t) + line.P1 * t;
        }

        public static Line3f[] SplitAt(Line3f line, float t)
        {
            if (t < 0f)
                t = 0f;
            if (t > 1f)
                t = 1f;

            Vector3f midpoint = line.Point(t);
            return new[]
            {
                new Line3f(line.P0, midpoint), new Line3f(midpoint, line.P1)
            };
        }

        public static float Length(Line3f line)
        {
            return (line.P1 - line.P0).Length;
        }

        public Vector3f Point(float t)
        {
            return Point(this, t);
        }

        public Line3f[] SplitAt(float t)
        {
            return SplitAt(this, t);
        }

        public float Length()
        {
            return Length(this);
        }

        public override string ToString()
        {
            return String.Format("{0}{2} {1}{2}", P0.ToString(), P1.ToString(), listSeparator);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = this.P0.GetHashCode();
                hashCode = (hashCode * 397) ^ this.P1.GetHashCode();
                return hashCode;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Line3f))
                return false;

            return this.Equals((Line3f)obj);
        }

        public bool Equals(Line3f other)
        {
            return
                P0 == other.P0 &&
                P1 == other.P1;
        }

        #region Operators

        public static Line3f operator +(Line3f line, Vector3f vector)
        {
            line.P0 += vector;
            line.P1 += vector;
            return line;
        }

        public static Line3f operator +(Vector3f vector, Line3f line)
        {
            line.P0 += vector;
            line.P1 += vector;
            return line;
        }

        public static Line3f operator -(Line3f line, Vector3f vector)
        {
            line.P0 -= vector;
            line.P1 -= vector;
            return line;
        }

        public static Line3f operator -(Vector3f vector, Line3f line)
        {
            line.P0 -= vector;
            line.P1 -= vector;
            return line;
        }

        public static bool operator ==(Line3f left, Line3f right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Line3f left, Line3f right)
        {
            return !left.Equals(right);
        }

        #endregion
    }
}
